<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xsl:output method="text" indent="no" encoding="windows-1251"/>
	
<xsl:template name="Tab">
	<xsl:text>&#x9;</xsl:text>
</xsl:template>

<xsl:template name="NewLine">
	<xsl:text>&#xD;&#xA;</xsl:text>
</xsl:template>

<xsl:template match="*" mode="normalize">
	<xsl:variable name="text">
		<xsl:apply-templates/>
	</xsl:variable>
	<xsl:value-of select="normalize-space(translate(translate($text, '&#xD;', ''), '&#xA;', ''))"/>
</xsl:template>

<xsl:template match="/">
	<xsl:apply-templates select="/xhtml:html/xhtml:body/xhtml:table[14]/xhtml:tr[position() > 1]" mode="points"/>
</xsl:template>

<xsl:template match="xhtml:tr" mode="points">
	<xsl:apply-templates select="xhtml:td[2]" mode="normalize"/>
	<xsl:call-template name="Tab"/>
	<xsl:apply-templates select="xhtml:td[2]" mode="pointType"/>
	<xsl:call-template name="Tab"/>
	<xsl:apply-templates select="xhtml:td[3]" mode="normalize"/>
	<xsl:call-template name="Tab"/>
	<xsl:apply-templates select="xhtml:td[4]" mode="normalize"/>
	<xsl:call-template name="Tab"/>
	<xsl:apply-templates select="xhtml:td[7]" mode="normalize"/>
	<xsl:call-template name="NewLine"/>
</xsl:template>

<xsl:template match="xhtml:td" mode="pointType">
	<xsl:choose>
		<!-- Платформы -->
		<xsl:when test="not(@bgcolor = '#00FF00') and (descendant::xhtml:font[@size=1])">
			<xsl:value-of select="'п'"/>
		</xsl:when>
		<!-- Мосты -->
		<xsl:when test="@bgcolor='#00FFFF'">
			<xsl:value-of select="'м'"/>
		</xsl:when>
		<!-- Туннели -->
		<xsl:when test="descendant::xhtml:img[@src='tonnel.gif']">
			<xsl:value-of select="'т'"/>
		</xsl:when>
		<!-- Станции -->
		<xsl:otherwise>
			<xsl:value-of select="'с'"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
