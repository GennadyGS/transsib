call %~dp0\pathSettings.cmd
SET OUT_FILE_NAME=%OUT_PATH%\%~n1.txt

if exist %OUT_FILE_NAME% del %OUT_FILE_NAME%
for /F "tokens=1 delims=	" %%s in (%1) do call :processSection %%s %2
goto :eof

:processSection
type %SECTIONS_OUT_PATH%\%~n1.txt >> %OUT_FILE_NAME%
if not "%2" == "" echo %2 >> %OUT_FILE_NAME%
goto :eof
