<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xsl:output method="text" indent="no" encoding="windows-1251"/>

<xsl:template name="Tab">
	<xsl:text>&#x9;</xsl:text>
</xsl:template>

<xsl:template name="NewLine">
	<xsl:text>&#xD;&#xA;</xsl:text>
</xsl:template>

<xsl:template match="*" mode="normalize">
	<xsl:variable name="text">
		<xsl:apply-templates/>
	</xsl:variable>
	<xsl:value-of select="normalize-space(translate(translate($text, '&#xD;', ''), '&#xA;', ''))"/>
</xsl:template>

<xsl:template match="/">
	<xsl:apply-templates select="/xhtml:html/xhtml:body/xhtml:table[10]/xhtml:tr/xhtml:td[4]/xhtml:strong/xhtml:font/xhtml:a" mode="section"/>
</xsl:template>

<xsl:template match="xhtml:a" mode="section">
	<xsl:value-of select="@href"/>
	<xsl:call-template name="Tab"/>
	<xsl:apply-templates select="." mode="normalize"/>
	<xsl:call-template name="NewLine"/>
</xsl:template>
	
</xsl:stylesheet>
