SET PROXY_SETTINGS_FILE=ProxySettings.cmd

SET BIN_DIR=Bin
SET CURL_TOOL_PATH=%BIN_DIR%\curl\curl.exe
SET HTML2XHTML_TOOL_PATH=%BIN_DIR%\html2xhtml\html2xhtml.exe 
SET MSXSL_TOOL_PATH=%BIN_DIR%\msxsl\msxsl.exe

SET BASEURL=http://www.transsib.ru
SET WAY_LEGEND=way-legend

SET LOAD_INTERVAL=3

call %~dp0\pathSettings.cmd

if exist %PROXY_SETTINGS_FILE% call %PROXY_SETTINGS_FILE%
md %OUT_PATH%
if defined Proxy set CurlProxyParams=--proxy %Proxy%
call :ProcessFile %BASEURL%/%WAY_LEGEND%.htm way-legend.xsl %OUT_PATH%
md %SECTIONS_OUT_PATH%
for /F "tokens=1 delims=	" %%s in (%OUT_PATH%\%WAY_LEGEND%.txt) do call :ProcessFile %BASEURL%/%%s section.xsl %SECTIONS_OUT_PATH%
goto :EOF

:ProcessFile
SET OUTPUT_HTML_FILE=%3\%~nx1
SET OUTPUT_XHTML_FILE=%3\%~n1.xhtml
SET OUTPUT_TXT_FILE=%3\%~n1.txt
%CURL_TOOL_PATH% %1 -o %OUTPUT_HTML_FILE% %CurlProxyParams%
%HTML2XHTML_TOOL_PATH% %OUTPUT_HTML_FILE% -o %OUTPUT_XHTML_FILE%
%MSXSL_TOOL_PATH% %OUTPUT_XHTML_FILE% %2 -o %OUTPUT_TXT_FILE%
timeout /t %LOAD_INTERVAL% /nobreak
goto :EOF

